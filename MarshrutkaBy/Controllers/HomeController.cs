﻿using MarshrutkaBy.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarshrutkaBy.Controllers
{
    public class HomeController : Controller
    {
        Models.ApplicationDbContext db = new Models.ApplicationDbContext();
        public static int idDataRoute;
        public static int idOrder;

        
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Index", "Admin");
            }

            else if (User.IsInRole("Company"))
            {
                return RedirectToAction("Index", "Company");
            }
            else if (User.IsInRole("Driver"))
            {
                return RedirectToAction("Index", "Driver");
            }
            return View("Index");
        }

        public ActionResult SearchRoutes(Models.DataRoute datemodels)
        {
            if (ModelState.IsValid == false)
            {
                if (datemodels.Date >= DateTime.Now.Date)
                {
                    var drs = db.DataRoute.Where(x => x.Date == datemodels.Date && x.Routes.StartingPoint == datemodels.Routes.StartingPoint && x.Routes.EndPoint == datemodels.Routes.EndPoint);

                     
                    return View(drs.ToList());
                }
            }
            return View();
        }

        public ActionResult Search()
        {
            Models.DataRoute drs = db.DataRoute.Find(idDataRoute);
            var dr = db.DataRoute.Where(x => x.Date == drs.Date && x.Routes.StartingPoint == drs.Routes.StartingPoint && x.Routes.EndPoint == drs.Routes.EndPoint);

            return View("SearchRoutes", dr.ToList());
        }

        public ActionResult WayPoints(int id)
        {
            var wayPoints = db.WayPoints.Where(x => x.IdRoute == id).ToList();
            return PartialView(wayPoints);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Order(int id)
        {
            var route = db.DataRoute.Where(x => x.IdDataRoute == id).ToList();
            ViewBag.routes = route;
            idDataRoute = id;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Order(Models.RegistrationAndRoutes rar)
        {

            if (ModelState.IsValid == true)
            {
                this.db.Registration.Add(rar.Registration);
                this.db.SaveChanges();

                ViewBag.reg = db.Registration.Where(x => x.IdRegistration == rar.Registration.IdRegistration).ToList();
                ViewBag.regroute = db.DataRoute.Where(x => x.IdDataRoute == idDataRoute).ToList();


                int idreg = rar.Registration.IdRegistration;
                DateTime date1 = DateTime.Now;
                var order = new Orders
                {
                    IdDataRoute = idDataRoute,
                    IdRegistration = idreg,
                    OrderTime = date1,
                    Status = false,
                    IdUser = User.Identity.GetUserId()
                };

                this.db.Order.Add(order);
                this.db.SaveChanges();

                var ord = db.Order.Where(x => x.IdRegistration == rar.Registration.IdRegistration).ToList();
                int id = rar.Registration.IdRegistration;
                idOrder = id;
                return RedirectToAction("Confirmation", new { id = id });
            }
            else return View();
        }

        public ActionResult Confirmation(int id)
        {
            var ors = db.Order.Where(x => x.IdRegistration == id).ToList();
            ViewBag.idreg = id;
            return View(ors);
        }

        [HttpPost]
        public ActionResult Confirmation(int idreg, int idroute)
        {
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            Models.Orders ors = db.Order.FirstOrDefault(x => x.IdRegistration == id);
            ViewBag.idregdel = id;
            return PartialView(ors);
        }

        public ActionResult Del(int id)
        {
            Models.Orders ors = db.Order.FirstOrDefault(x => x.IdRegistration == id);
            this.db.Order.Remove(ors);
            this.db.SaveChanges();

            return View("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Models.Orders os = db.Order.FirstOrDefault(x => x.IdRegistration == id);

            ViewBag.order = db.Order.Where(x => x.Registration.IdRegistration == id).ToList();

            return View("Edit", os);
        }

        [HttpPost]
        public ActionResult Edit(Orders os)
        {
            var edit = db.Registration.FirstOrDefault(x => x.IdRegistration == os.IdRegistration);

            edit.LastName = os.Registration.LastName.ToString();
            edit.FirstName = os.Registration.FirstName.ToString();
            edit.MiddleName = os.Registration.MiddleName.ToString();

            db.SaveChanges();

            var ord = db.Order.Where(x => x.IdRegistration == os.IdRegistration).ToList();
            return View("Confirmation", ord);
        }


        public ActionResult Home()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}