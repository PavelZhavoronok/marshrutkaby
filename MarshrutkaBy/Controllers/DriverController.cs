﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MarshrutkaBy.Models;

namespace MarshrutkaBy.Controllers
{
    public class DriverController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

       
        [Authorize]
        public ActionResult Index()
        {
            var IdUser = User.Identity.GetUserId();
            Drivers driver = db.Drivers.FirstOrDefault(x => x.UserId == IdUser);
            return View(driver);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Edit()
        {
            var IdUser = User.Identity.GetUserId();
            Drivers driver = db.Drivers.FirstOrDefault(x => x.UserId == IdUser);
            return View(driver);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(Drivers driver)
        {
            var IdUser = User.Identity.GetUserId();
            var edit = db.Drivers.FirstOrDefault(x => x.UserId == IdUser);

            edit.LastName = driver.LastName;
            edit.FirstName = driver.FirstName;
            edit.Birthday = driver.Birthday;
            edit.Email = driver.Email;
            edit.License = driver.License;
            edit.NumberPhone = driver.NumberPhone;

            db.SaveChanges();

            return RedirectToAction("Index");
        }

      
        public ActionResult UpcomingTrips()
        {
            var date = DateTime.Now.Date;
            var time = DateTime.Now.TimeOfDay;
            var IdUser = User.Identity.GetUserId();
            var IdDriver = db.Drivers.FirstOrDefault(x => x.UserId == IdUser);

            var dataRoute1 = db.DataRoute.Where(x => x.IdDriver == IdDriver.IdDriver & x.Date > date);
            var dataRoute2 = db.DataRoute.Where(x => x.IdDriver == IdDriver.IdDriver & x.Date == date && x.Times.ArrivalTime >= time);
            var dataRoute = dataRoute1.Concat(dataRoute2);

            if (dataRoute.Count() != 0)
            {
                return PartialView(dataRoute.ToList());
            }
            return PartialView();
        }

       
        public ActionResult CommitedTrips()
        {
            var date = DateTime.Now.Date;
            var time = DateTime.Now.TimeOfDay;
            var IdUser = User.Identity.GetUserId();
            var IdDriver = db.Drivers.FirstOrDefault(x => x.UserId == IdUser);

            var dataRoute1 = db.DataRoute.Where(x => x.IdDriver == IdDriver.IdDriver & x.Date < date);
            var dataRoute2 = db.DataRoute.Where(x => x.IdDriver == IdDriver.IdDriver & x.Date == date && x.Times.ArrivalTime <= time);
            var dataRoute = dataRoute1.Concat(dataRoute2);

            if (dataRoute.Count() != 0)
            {
                return PartialView(dataRoute.ToList());
            }
            return PartialView();
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            var order = db.Order.Where(x => x.IdDataRoute == id && x.Verification == "Подтверждено").ToList();

            if (order.Count() != 0)
            {
                return View(order);
            }
            return View();
        }

        [HttpPost]
        public ActionResult EditStatus(int id)
        {
            var order = db.Order.Find(id);
            if(order.Status == true)
            {
                order.Status = false;
            }
            else
            {
                order.Status = true;
            }

            db.SaveChanges();

            return RedirectToAction("Details", id);
        }

       
    }
}