﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MarshrutkaBy.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace MarshrutkaBy.Controllers
{
    public class AdminController : Controller
    {
        Models.ApplicationDbContext db = new Models.ApplicationDbContext();
        public static int IDCompany;
        private ApplicationUserManager _userManager;

        // GET: Admin
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.countTC = db.TransportCompanies.Count();
            ViewBag.countRoute = db.Routes.Count();
            ViewBag.countOrder = db.Order.Count();
            ViewBag.countCar = db.Cars.Count();
            ViewBag.countUser = db.Users.Count();

            return View();
        }
        public ActionResult Company()
        {
            var TC = db.TransportCompanies.ToList();
            return View(TC);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Models.TransportCompanies tc = db.TransportCompanies.FirstOrDefault(x => x.IdTransportCompanies == id);
            return View(tc);
        }

        [HttpPost]
        public ActionResult Edit(Models.TransportCompanies tcs)
        {
            var edit = db.TransportCompanies.FirstOrDefault(x => x.IdTransportCompanies == tcs.IdTransportCompanies);

            edit.Name = tcs.Name.ToString();
            edit.NumberPhone = tcs.NumberPhone.ToString();
            edit.Email = tcs.NumberPhone.ToString();

            db.SaveChanges();

            return RedirectToAction("Company");
        }

        public ActionResult Details(int id)
        {
            var company = db.TransportCompanies.Find(id);
            ViewBag.IdTC = id;
            return View(company);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Models.TransportCompanies tcs)
        {
            this.db.TransportCompanies.Add(tcs);
            this.db.SaveChanges();
            IDCompany = tcs.IdTransportCompanies;
            return RedirectToAction("CreateAdminCompany", "Admin");
        }

        [HttpGet]
        public ActionResult CreateAdminCompany()
        {
            return View();
        }

        #region CreateAdminCompany

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public async System.Threading.Tasks.Task<ActionResult> CreateAdminCompany(Models.RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, "Company");

                    var adminTC = new AdminTransportCompanies
                    {
                        UserId = user.Id,
                        IdTransportCompanies = IDCompany
                    };

                    this.db.AdminTransportCompanies.Add(adminTC);
                    this.db.SaveChanges();

                    return RedirectToAction("Company", "Admin");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        #endregion

        public ActionResult Cars(int id)
        {
            //var cars = db.Cars.Where(x => x == id).ToList();
            var cars = db.Cars.Where(x => x.IdTransportCompanies == id.ToString());
            return PartialView(cars.ToList());
        }

        public ActionResult Drivers(int id)
        {
            var drivers = db.Drivers.Where(x => x.IdTransportCompanies == id.ToString());
            return PartialView(drivers.ToList());
        }

        public ActionResult Routes(int id)
        {
            var routes = db.Routes.Where(x => x.IdTransportCompanies == id.ToString());
            return PartialView(routes.ToList());
        }

        public ActionResult Delete(int id)
        {
            ViewBag.IdTc = id;
            Models.TransportCompanies tc = db.TransportCompanies.FirstOrDefault(x => x.IdTransportCompanies == id);
            return PartialView(tc);
        }


        public ActionResult DeleteTC(int id)
        {



            Models.TransportCompanies tcs = db.TransportCompanies.FirstOrDefault(x => x.IdTransportCompanies == id);
            this.db.TransportCompanies.Remove(tcs);
            this.db.SaveChanges();

            var adminTC = db.AdminTransportCompanies.Where(i => i.IdTransportCompanies == id).ToList();

            foreach (var admin in adminTC)
            {
                var adm = db.Users.Find(admin.UserId);
                this.db.Users.Remove(adm);
                this.db.SaveChanges();
            }

            return RedirectToAction("Company");
        }
    }
}