﻿using MarshrutkaBy.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace MarshrutkaBy.Controllers
{
    public class CompanyController : Controller
    {
        private const string Verification = "Подтверждение бронирования";
        private const string NonVerification = "Анулирование бронирования";
        Models.ApplicationDbContext db = new Models.ApplicationDbContext();
        public static string idCurrentUser;
        private ApplicationUserManager _userManager;
        private static int idDataroute;
        // GET: Company
        public int GetIdCompany()
        {
            idCurrentUser = User.Identity.GetUserId();
            var admincompany = db.AdminTransportCompanies.Find(idCurrentUser);
            return admincompany.IdTransportCompanies;
        }

        public ActionResult Index()
        {
            if (GetIdCompany() != 0)
            {
                var company = db.TransportCompanies.Find(GetIdCompany());
                return View(company);
            }
            return View();
        }

        

        public ActionResult NonVerificatedOrder()
        {
            var idTC = GetIdCompany();
            //var dataRoutes = db.DataRoute.Where(x => x.IdTransportCompanies == idTC)
            
            var order = db.Order.Where(x => x.DataRoute.IdTransportCompanies == idTC & x.Verification == null);
          

            if (order.Count() != 0)
            {
                return PartialView(order.ToList());
            }
            return PartialView();
        }

        public async Task<ActionResult> Verificated(int id)
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + @"\verification.txt";
            var order1 = db.Order.Find(id);
            var email = db.Users.Find(order1.IdUser);
            order1.Verification = "Подтверждено";
            db.SaveChanges();

            await SendEmailAsync(email.Email, Verification, BodyText(filePath, order1.IdOrder, Convert.ToString(order1.OrderTime), order1.DataRoute.Cars.ModelCars + " " + order1.DataRoute.Cars.NumberPlate, order1.DataRoute.Routes.StartingPoint + " - " + order1.DataRoute.Routes.EndPoint, Convert.ToString(order1.DataRoute.Date + " " + order1.DataRoute.Times.DepartureTime), order1.DataRoute.Price));

            return RedirectToAction("Routes");
        }

        public async Task<ActionResult> NonVerificated(int id)
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + @"\nonverification.txt";
            var order1 = db.Order.Find(id);
            var email = db.Users.Find(order1.IdUser);
            order1.Verification = "Отменено";
            db.SaveChanges();

            await SendEmailAsync(email.Email, NonVerification, BodyText(filePath, order1.IdOrder, Convert.ToString(order1.OrderTime), order1.DataRoute.Cars.ModelCars + " " + order1.DataRoute.Cars.NumberPlate, order1.DataRoute.Routes.StartingPoint + " - " + order1.DataRoute.Routes.EndPoint, Convert.ToString(order1.DataRoute.Date + " " + order1.DataRoute.Times.DepartureTime), order1.DataRoute.Price));

            return RedirectToAction("Routes");
        }

        public string BodyText(string filePath, int idOrder, string date, string modelAndNumberOfPalte, string routes, string dateRoute, int price)
        {

            StreamReader file = new StreamReader(filePath, Encoding.Default, true);
            var text = String.Format(file.ReadToEnd(), idOrder, date, modelAndNumberOfPalte, routes, dateRoute, price);

            return text;
        }

        private static async Task SendEmailAsync(string email, string subject, string text)
        {
            MailAddress from = new MailAddress("pahazavoronok@gmail.com", "Marshrutka.by");
            MailAddress to = new MailAddress(email);
            MailMessage m = new MailMessage(from, to)
            {
                Subject = subject,
                Body = text
            };
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("pahazavoronok@gmail.com", "11235813213455Paha"),
                EnableSsl = true
            };
            try
            {
                await smtp.SendMailAsync(m);
            }
            catch (SmtpException ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }

        }

        public ActionResult Details(int id)
        {
            var order = db.Order.Where(x => x.IdDataRoute == id && x.Verification == "Подтверждено").ToList();

            if (order.Count() != 0)
            {
                return View(order);
            }
            return View();
        }

        #region Route

       [HttpGet]
        public ActionResult CreateRoute()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            List<SelectListItem> itemsdrivers = new List<SelectListItem>();
            List<SelectListItem> itemscars = new List<SelectListItem>();
            var idTC = GetIdCompany().ToString();
            var routes = db.Routes.Where(x => x.IdTransportCompanies == idTC).ToList();
            var drivers = db.Drivers.Where(x => x.IdTransportCompanies == idTC).ToList();
            var cars = db.Cars.Where(x => x.IdTransportCompanies == idTC).ToList();

            foreach (var route in routes)
            {
                items.Add(new SelectListItem { Text = route.StartingPoint + "-" + route.EndPoint, Value = route.IdRoute.ToString() });
            }
            if (items != null)
            {
                ViewData["Course"] = items;
            }
            else
            {
                items.Add(new SelectListItem { Text = "Нет доступных направлений", Value = "-1" });
                ViewData["Course"] = items;
            }

            foreach (var driver in drivers)
            {
                itemsdrivers.Add(new SelectListItem { Text = driver.FirstName + " " + driver.LastName, Value = driver.IdDriver.ToString() });
            }
            if (itemsdrivers != null)
            {
                ViewData["Driver"] = itemsdrivers;
            }
            else
            {
                itemsdrivers.Add(new SelectListItem { Text = "Нет доступных водителей", Value = "-1" });
                ViewData["Driver"] = itemsdrivers;
            }
            foreach (var car in cars)
            {
                itemscars.Add(new SelectListItem { Text = car.ModelCars + " " + car.NumberPlate, Value = car.IdCar.ToString() });
            }
            if (itemscars != null)
            {
                ViewData["Car"] = itemscars;
            }
            else
            {
                itemscars.Add(new SelectListItem { Text = "Нет доступных маршруток", Value = "-1" });
                ViewData["Car"] = itemscars;
            }

            return View();
        }

        [HttpPost]
        public ActionResult CreateRoute(Models.DataRoute dataRoutes, Models.Times timees, string course, string driver, string car)
        {
            if (ModelState.IsValid == true)
            {
                var time = new Times
                {
                    ArrivalTime = dataRoutes.Times.ArrivalTime,
                    DepartureTime = dataRoutes.Times.DepartureTime
                };
                this.db.Time.Add(time);
                db.SaveChanges();

                var dataRoute = new DataRoute
                {
                    IdCar = Convert.ToInt32(car),
                    IdDriver = Convert.ToInt32(driver),
                    IdRoute = Convert.ToInt32(course),
                    IdTransportCompanies = GetIdCompany(),
                    IdTime = time.IdTime,
                    Price = dataRoutes.Price,
                    Date = dataRoutes.Date.Date,
                    Information = dataRoutes.Information
                };
                this.db.DataRoute.Add(dataRoute);
                db.SaveChanges();

                idDataroute = dataRoute.IdDataRoute;
            }
            Session["IdRoute"] = Convert.ToInt32(course);
            return RedirectToAction("DetailsWayPoints");
        }

        public ActionResult CreateWayPoints(int id)
        {
            ViewBag.idRoute = id;
            Session["Id"] = id;
            return PartialView();
        }

        [HttpPost]
        public ActionResult WayPoints(WayPoints waypoints)
        {
            int id = Convert.ToInt32(Session["Id"]);
            var wpt = new WayPointsTime
            {
                DepartureTime = waypoints.WayPointsTime.DepartureTime,
                ArrivalTime = waypoints.WayPointsTime.ArrivalTime
            };
            this.db.WayPointsTime.Add(wpt);
            db.SaveChanges();

            var time = db.WayPointsTime.FirstOrDefault(x => x.ArrivalTime == waypoints.WayPointsTime.ArrivalTime & x.DepartureTime == waypoints.WayPointsTime.DepartureTime);

            var wp = new WayPoints
            {
                NameWayPoint = waypoints.NameWayPoint,
                IdRoute = id,
                IdWayPointsTime = time.IdWayPointsTime
            };
            this.db.WayPoints.Add(wp);
            db.SaveChanges();


            var wayPoints = db.WayPoints.Where(x => x.IdRoute == id).ToList();
            return PartialView(wayPoints);
        }

        public ActionResult WayPoints(int id)
        {
            var wayPoints = db.WayPoints.Where(x => x.IdRoute == id).ToList();
            return PartialView(wayPoints);
        }

        public ActionResult DetailsWayPoints()
        {
            int idRoute = Convert.ToInt32(Session["IdRoute"]);

            DataRoute dateroute = db.DataRoute.Find(idDataroute);

            return View(dateroute);
        }



        public ActionResult Routes()
        {
            var idTC = GetIdCompany();
            ViewBag.IdTC = idTC.ToString();
            var date = DateTime.Now.Date;
            var time = DateTime.Now.TimeOfDay;
            var route = db.Routes.Find(idTC);

            if (route != null)
            {
                var dataRoute1 = db.DataRoute.Where(x => x.IdRoute == route.IdRoute & x.Date > date);
                var dataRoute2 = db.DataRoute.Where(x => x.IdRoute == route.IdRoute & x.Date == date && x.Times.ArrivalTime >= time);
                var dataRoute = dataRoute1.Concat(dataRoute2);
                return View(dataRoute.ToList());
            }
            return View();
        }

        public ActionResult CommitedRoute()
        {
            var idTC = GetIdCompany();
            var date = DateTime.Now.Date;
            var time = DateTime.Now.TimeOfDay;
            var route = db.Routes.Find(idTC);

            if (route != null)
            {
                var dataRoute1 = db.DataRoute.Where(x => x.IdRoute == route.IdRoute & x.Date < date);
                var dataRoute2 = db.DataRoute.Where(x => x.IdRoute == route.IdRoute & x.Date == date && x.Times.ArrivalTime <= time);
                var dataRoute = dataRoute1.Concat(dataRoute2);
                return PartialView(dataRoute.ToList());
            }
            return View();
        }

        public ActionResult EditRoute(int id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            List<SelectListItem> itemsdrivers = new List<SelectListItem>();
            List<SelectListItem> itemscars = new List<SelectListItem>();

            var dataroute = db.DataRoute.Find(id);

            var routes = db.Routes.Where(x => x.IdRoute == dataroute.IdRoute).ToList();
            var drivers = db.Drivers.Where(x => x.IdDriver == dataroute.IdDriver).ToList();
            var cars = db.Cars.Where(x => x.IdCar == dataroute.IdCar).ToList();

            foreach (var route in routes)
            {
                items.Add(new SelectListItem { Text = route.StartingPoint + "-" + route.EndPoint, Value = route.IdRoute.ToString() });
            }
            if (items != null)
            {
                ViewData["Course"] = items;
            }
            else
            {
                items.Add(new SelectListItem { Text = "Нет доступных направлений", Value = "-1" });
                ViewData["Course"] = items;
            }

            foreach (var driver in drivers)
            {
                itemsdrivers.Add(new SelectListItem { Text = driver.FirstName + " " + driver.LastName, Value = driver.IdDriver.ToString() });
            }
            if (itemsdrivers != null)
            {
                ViewData["Driver"] = itemsdrivers;
            }
            else
            {
                itemsdrivers.Add(new SelectListItem { Text = "Нет доступных водителей", Value = "-1" });
                ViewData["Driver"] = itemsdrivers;
            }
            foreach (var car in cars)
            {
                itemscars.Add(new SelectListItem { Text = car.ModelCars + " " + car.NumberPlate, Value = car.IdCar.ToString() });
            }
            if (itemscars != null)
            {
                ViewData["Car"] = itemscars;
            }
            else
            {
                itemscars.Add(new SelectListItem { Text = "Нет доступных маршруток", Value = "-1" });
                ViewData["Car"] = itemscars;
            }

            return View(dataroute);
        }

        [HttpPost]
        public ActionResult EditRoute(Models.DataRoute dataRoutes, Models.Times times, string course, string driver, string car)
        {
            if (ModelState.IsValid == true)
            {
                var dataroute = db.DataRoute.Find(dataRoutes.IdDataRoute);
                var time = db.Time.Find(dataroute.IdTime);

                time.ArrivalTime = times.ArrivalTime;
                time.DepartureTime = times.DepartureTime;
                dataroute.IdCar = Convert.ToInt32(car);
                dataroute.IdDriver = Convert.ToInt32(driver);
                dataroute.IdRoute = Convert.ToInt32(course);
                dataroute.IdTransportCompanies = GetIdCompany();
                dataroute.IdTime = time.IdTime;
                dataroute.Price = dataRoutes.Price;
                dataroute.Date = dataRoutes.Date.Date;
                dataroute.Information = dataRoutes.Information;

                db.SaveChanges();
            }
            return RedirectToAction("Routes");
        }

        public ActionResult DeleteRoute(int id)
        {
            var dataroute = db.DataRoute.Find(id);
            return PartialView(dataroute);
        }

        #endregion

        #region Course

        public ActionResult Course()
        {
            var idTC = GetIdCompany().ToString();
            var routes = db.Routes.Where(x => x.IdTransportCompanies == idTC).ToList();
            return PartialView(routes);
        }

        [HttpGet]
        public ActionResult CreateCourse()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateCourse(Models.Routes routes)
        {
            if (ModelState.IsValid == false)
            {
                var route = new Routes
                {
                    StartingPoint = routes.StartingPoint,
                    EndPoint = routes.EndPoint,
                    Distance = routes.Distance,
                    Changes = routes.Changes,
                    IdTransportCompanies = GetIdCompany().ToString()
                };
                this.db.Routes.Add(route);
                db.SaveChanges();
            }
            return RedirectToAction("Routes");
        }

        [HttpGet]
        public ActionResult EditCourse(int id)
        {
            var course = db.Routes.Find(id);
            return View(course);
        }

        [HttpPost]
        public ActionResult EditCourse(Routes routes)
        {
            var route = db.Routes.Find(routes.IdRoute);
            route.StartingPoint = routes.StartingPoint;
            route.EndPoint = routes.EndPoint;
            route.Distance = routes.Distance;
            route.Changes = routes.Changes;

            db.SaveChanges();
            
            return RedirectToAction("Routes");
        }

        [HttpGet]
        public ActionResult DeleteCourse(int id)
        {
            var course = db.Routes.Find(id);
            return PartialView(course);
        }

        public ActionResult DelCourse(int id)
        {
            var car = db.Routes.Find(id);
            this.db.Routes.Remove(car);
            this.db.SaveChanges();
            return RedirectToAction("Routes");
        }

        #endregion

        #region Driver

        [HttpGet]
        public ActionResult Drivers()
        {
            var idtc = GetIdCompany().ToString();
            var drivers = db.Drivers.Where(x => x.IdTransportCompanies == idtc);
            return View(drivers);
        }

        [HttpGet]
        public ActionResult CreateDriver()
        {

            return View();
        }

        [HttpPost]
        public ActionResult CreateDriver(Models.Drivers drivers)
        {
            if (ModelState.IsValid == false)
            {
                var driver = new Drivers
                {
                    LastName = drivers.LastName,
                    FirstName = drivers.FirstName,
                    Birthday = drivers.Birthday.Date,
                    License = drivers.License,
                    NumberPhone = drivers.NumberPhone,
                    Email = drivers.Email,
                    Raiting = drivers.Raiting,
                    Photo = drivers.Photo,
                    IdTransportCompanies = GetIdCompany().ToString(),
                    UserId = TempData["UserId"].ToString()

                };

                this.db.Drivers.Add(driver);
                db.SaveChanges();
            }
            return RedirectToAction("Drivers");
        }

        [HttpGet]
        public ActionResult EditDriver(int id)
        {
            var driver = db.Drivers.Find(id);
            return View(driver);
        }

        [HttpPost]
        public ActionResult EditDriver(Drivers driver)
        {
            var dr = db.Drivers.Find(driver.IdDriver);

            if (ModelState.IsValid == false)
            {
                dr.LastName = driver.LastName;
                dr.FirstName = driver.FirstName;
                dr.Birthday = driver.Birthday.Date;
                dr.Email = driver.Email;
                dr.License = driver.License;
                dr.NumberPhone = driver.NumberPhone;
                dr.Raiting = driver.Raiting;
                dr.Photo = dr.Photo;

                db.SaveChanges();
                return RedirectToAction("Drivers");
            }
            return View();

        }
        [HttpGet]
        public ActionResult DeleteDriver(int id)
        {
            var driver = db.Drivers.Find(id);
            return PartialView(driver);
        }

      
        public ActionResult DelDriver(int id)
        {
            var dr = db.Drivers.Find(id);
            this.db.Drivers.Remove(dr);
            this.db.SaveChanges();
            return RedirectToAction("Drivers");
        }

        #endregion

        #region CreateDriverAccount

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public async System.Threading.Tasks.Task<ActionResult> CreateDriverAccount(Models.RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user.Id, "Driver");
                    TempData["UserId"] = user.Id; 
                    return RedirectToAction("CreateDriver", "Company");
                }
                AddErrors(result);
            }

            return View(model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        #endregion

        #region Car

        public ActionResult Cars()
        {
            var idtc = GetIdCompany().ToString();
            var cars = db.Cars.Where(x => x.IdTransportCompanies == idtc);
            return View(cars);
        }

        [HttpGet]
        public ActionResult CreateCar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCar(Models.Cars cars)
        {
            var car = new Cars
            {
                Color = cars.Color,
                IdTransportCompanies = GetIdCompany().ToString(),
                ModelCars = cars.ModelCars,
                NumberOfSeats = cars.NumberOfSeats,
                NumberPlate = cars.NumberPlate,
                Photo = cars.Photo,
                Services = cars.Services,
            };
            this.db.Cars.Add(car);
            db.SaveChanges();
            return RedirectToAction("Cars");
        }

        [HttpGet]
        public ActionResult EditCar(int id)
        {
            var car = db.Cars.Find(id);
            return View(car);
        }

        [HttpPost]
        public ActionResult EditCar(Cars cars)
        {
            var car = db.Cars.Find(cars.IdCar);

            car.ModelCars = cars.ModelCars;
            car.NumberOfSeats = cars.NumberOfSeats;
            car.Services = cars.Services;
            car.Photo = cars.Photo;
            car.NumberPlate = cars.NumberPlate;
            car.Color = cars.Color;

            db.SaveChanges();

            return RedirectToAction("Cars");
        }

        public ActionResult DeleteCar(int id)
        {
            var car = db.Cars.Find(id);
            return PartialView(car);
        }

        public ActionResult DelCar(int id)
        {
            var car = db.Cars.Find(id);
            this.db.Cars.Remove(car);
            this.db.SaveChanges();
            return RedirectToAction("Cars");
        }

        #endregion
    }
}