﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarshrutkaBy.Models
{
    public class RegistrationAndRoutes
    {
        public DataRoute DataRoute { get; set; }
        public Registration Registration { get; set; }
    }
}