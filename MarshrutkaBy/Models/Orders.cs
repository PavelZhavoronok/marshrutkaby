﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



namespace MarshrutkaBy.Models
{
    [Table("Orders")]
    public partial class Orders
    {
        [Key]
        [Display(Name = "Номер заказа")]
        public int IdOrder { get; set; }

        [Display(Name = "Дата заказа")]
        public DateTime OrderTime { get; set; }

        [Display(Name = "Пассажир прибыл")]
        public bool Status { get; set; }

        [Display(Name = "Статус заказа")]
        public string Verification { get; set; }

        public int IdDataRoute { get; set; }
        public int IdRegistration { get; set; }
        public string IdUser { get; set; }

        public virtual DataRoute DataRoute { get; set; }
        public virtual Registration Registration { get; set; }
    }
}