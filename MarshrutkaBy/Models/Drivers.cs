﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarshrutkaBy.Models
{
    [Table("Drivers")]
    public partial class Drivers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Drivers()
        {
            this.DataRoute = new HashSet<DataRoute>();
        }

        [Key]
        public int IdDriver { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        public string LastName { get; set; }

        [Display(Name = "Имя")]
        [Required]
        public string FirstName { get; set; }

        
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "День рождения")]
        public DateTime Birthday { get; set; }

        [Display(Name = "Лицензия")]
        [Required]
        public string License { get; set; }

        [Display(Name = "Номер телефона")]
        [Required]
        public string NumberPhone { get; set; }

        [Required]
        public string Email { get; set; }

        [Display(Name = "Рейтинг")]
        public int Raiting { get; set; }

        [Display(Name = "Фото")]
        public string Photo { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public string IdTransportCompanies { get; set; }

        public virtual TransportCompanies TransportCompanies { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DataRoute> DataRoute { get; set; }
    }
}