﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarshrutkaBy.Models
{
    [Table("Times")]
    public partial class Times
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Times()
        {
            this.DataRoute = new HashSet<DataRoute>();
        }

        [Key]
        public int IdTime { get; set; }

        [Display(Name = "Время отправления")]
        public TimeSpan DepartureTime { get; set; }

        [Display(Name = "Время прибытия")]
        public TimeSpan ArrivalTime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DataRoute> DataRoute { get; set; }
    }
}