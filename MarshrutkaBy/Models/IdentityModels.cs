﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MarshrutkaBy.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ApplicationDbContext>());
        }

        public virtual DbSet<Cars> Cars { get; set; }
        public virtual DbSet<DataRoute> DataRoute { get; set; }
        public virtual DbSet<Drivers> Drivers { get; set; }
        public virtual DbSet<Registration> Registration { get; set; }
        public virtual DbSet<Routes> Routes { get; set; }
        public virtual DbSet<Times> Time { get; set; }
        public virtual DbSet<Orders> Order { get; set; }
        public virtual DbSet<TransportCompanies> TransportCompanies { get; set; }
        public virtual DbSet<WayPoints> WayPoints { get; set; }
        public virtual DbSet<WayPointsTime> WayPointsTime { get; set; }
        public virtual DbSet<AdminTransportCompanies> AdminTransportCompanies { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}