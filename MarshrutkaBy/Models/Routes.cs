﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarshrutkaBy.Models
{
    [Table("Routes")]
    public partial class Routes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Routes()
        {
            this.DataRoute = new HashSet<DataRoute>();
            this.WayPoints = new HashSet<WayPoints>();
        }

        [Key]
        public int IdRoute { get; set; }

        [Required(ErrorMessage = "Введите место отправления")]
        [Display(Name = "Откуда")]
        public string StartingPoint { get; set; }

        [Required(ErrorMessage = "Введите место назначения")]
        [Display(Name = "Куда")]
        public string EndPoint { get; set; }

        [Display(Name = "Расстояние")]
        public int Distance { get; set; }

       
        [Display(Name = "Информация")]
        public string Changes { get; set; }

        [Required]
        public string IdTransportCompanies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WayPoints> WayPoints { get; set; }
        public virtual TransportCompanies TransportCompanies { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DataRoute> DataRoute { get; set; }
    }
}