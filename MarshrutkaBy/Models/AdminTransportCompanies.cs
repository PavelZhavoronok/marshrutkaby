﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MarshrutkaBy.Models
{
    public partial class AdminTransportCompanies
    {
        [Key]
        public string UserId { get; set; }
        public int IdTransportCompanies { get; set; }

        public virtual TransportCompanies TransportCompanies { get; set; }
    }
}