﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarshrutkaBy.Models
{
    [Table("Cars")]
    public partial class Cars
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cars()
        {
            this.DataRoute = new HashSet<DataRoute>();
        }

        [Key]
        public int IdCar { get; set; }

        [Required]
        [Display(Name = "Регистрационный номер")]
        public string NumberPlate { get; set; }

        [Required]
        [Display(Name = "Марка и модель авто")]
        public string ModelCars { get; set; }

        [Display(Name = "Цвет")]
        public string Color { get; set; }

        [Required]
        [Display(Name = "Доп. услуги")]
        public string Services { get; set; }

        [Display(Name = "Количество мест")]
        public int NumberOfSeats { get; set; }

        [Display(Name = "Фото")]
        public string Photo { get; set; }

        [Required]
        public string IdTransportCompanies { get; set; }

        public virtual TransportCompanies TransportCompanies { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DataRoute> DataRoute { get; set; }
    }
}