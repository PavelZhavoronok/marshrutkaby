﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarshrutkaBy.Models
{
    [Table("DataRoute")]
    public partial class DataRoute
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DataRoute()
        {
            this.Orders = new HashSet<Orders>();
        }

        [Key]
        public int IdDataRoute { get; set; }

        [Required(ErrorMessage = "Введите дату отправления")]
       
        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = false)]
        [Display(Name = "Дата отправления")]
        public DateTime Date { get; set; }

        [Display(Name = "Информация")]
        public string Information { get; set; }

        public int IdRoute { get; set; }

        public int IdTime { get; set; }

        [Display(Name = "Цена")]
        public int Price { get; set; }

        public int IdCar { get; set; }

        public int IdDriver { get; set; }

        public int IdTransportCompanies { get; set; }

        public virtual Cars Cars { get; set; }
        public virtual Times Times { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Orders> Orders { get; set; }
        public virtual Drivers Drivers { get; set; }
        public virtual Routes Routes { get; set; }
        public virtual TransportCompanies TransportCompanies { get; set; }
    }
}