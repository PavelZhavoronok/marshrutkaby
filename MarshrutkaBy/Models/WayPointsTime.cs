﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MarshrutkaBy.Models
{


    [Table("WayPointsTime")]
    public partial class WayPointsTime
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WayPointsTime()
        {
            this.WayPoints = new HashSet<WayPoints>();
        }

        [Key]
        public int IdWayPointsTime { get; set; }

        [Display(Name = "Время прибытия")]
        public System.TimeSpan DepartureTime { get; set; }

        [Display(Name = "Время отправления")]
        public System.TimeSpan ArrivalTime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WayPoints> WayPoints { get; set; }
    }
}