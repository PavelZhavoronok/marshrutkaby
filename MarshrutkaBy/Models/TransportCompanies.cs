﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarshrutkaBy.Models
{
    [Table("TransportCompanies")]
    public partial class TransportCompanies
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TransportCompanies()
        {
            this.AdminTransportCompanies = new HashSet<AdminTransportCompanies>();
            this.Cars = new HashSet<Cars>();
            this.Drivers = new HashSet<Drivers>();
            this.Routes = new HashSet<Routes>();
            this.DataRoute = new HashSet<DataRoute>();
        }

        [Key]
        public int IdTransportCompanies { get; set; }

        [Required]
        [Display(Name = "Перевозчик")]
        public string Name { get; set; }

        [Display(Name = "Номер телефона")]
        [Required]
        public string NumberPhone { get; set; }

        [Required]
        public string Email { get; set; }

        [Display(Name = "Рейтинг")]
        [Required]
        public string Raiting { get; set; }

        [Display(Name = "Описание")]
        [Required]
        public string Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdminTransportCompanies> AdminTransportCompanies { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cars> Cars { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Drivers> Drivers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Routes> Routes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DataRoute> DataRoute { get; set; }
    }
}