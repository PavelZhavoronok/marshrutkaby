﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarshrutkaBy.Models
{
    [Table("WayPoints")]
    public partial class WayPoints
    {
        [Key]
        public int IdWayPoints { get; set; }

        [Display(Name = "Название")]
        public string NameWayPoint { get; set; }

        public int IdWayPointsTime { get; set; }

        public int IdRoute { get; set; }

        public virtual Routes Routes { get; set; }

        public virtual WayPointsTime WayPointsTime { get; set; }
    }
}