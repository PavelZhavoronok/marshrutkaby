﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MarshrutkaBy.Startup))]
namespace MarshrutkaBy
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
